# 2. Features
Hysteria includes a good variety of features, including

- Aura for FireBalls, Mobiles, Hostiles, and Players
- WarnRadius will alert the user of nearby players
- Fullbright (lightmap; eased fadein/out)
- Caching Text Radar
- Friend Management
- Chat-Key binding management
- Enhanced movement in webs
- FishBans lookup utility
- Lexing of outgoing chat
- Fly: Planar and 3D
- Glide
- Arrow path landing prediction for other players
- Anti-Knockback
- Selective pickup rejection
- Horizontal phase, IE X/Z noclip
- Sneak: Packet/Catching/Non-Catching
- Run
- Projectile Prediction
- InstantBow
- Paralyze
- The ability to cancel rotation updates from the server
- Chat logging (via per-server data storage mechanism)
- Highly abstract API and other facilities/toolkits
