# 3.5. The Friends System #

Hysteria has a rather complicated friend system.

Each player has their own respective storage companions and property system with seamless deserialization/serialization.

## Every-day friends management

For all you filthy casuals, Hysteria is equipped with a simple friends management command, `fr` (or `friends`, `nl`, `namelist`).

For simplicity's sake, the following table details all flags, their arguments, and their functions.

|Flag               |Arguments                  |Function                                           |
|:------------------|:--------------------------|:--------------------------------------------------|
|list, *@if_none*   |                           |List all players currently loaded and modified     |
|nick, n            |`<real name> <nickname>`   |Nickname a player                                  |
|friendship, f      |`<real name> <friendship>` |Set friendship level                               |
|clearnick, cn      |`<real name>`              |Remove a player's nickname                         |

## Friends management on steroids

If you're a 24/7 linux user like Bowser, you'll know that the previously mentioned command is, in fact, for filthy casuals (those who use the Windows).

As such, there exists a magical command called `fru`, or `frutil`. This command was of course added before `fr`.

Usage chart:

|Flag               |Arguments                          |Function                                                       |
|:------------------|:----------------------------------|:--------------------------------------------------------------|
|list, *@if_none*   |                                   |List all players in the data cache pool                        |
|remove, r          |`<real name>`                      |Erase a player's data                                          |
|transprop, tp      |`<real name> <property> [value]`   |Get(2) or Set(3) a transient property for the player           |
|removetrans, rt    |`<real name> <property>`           |Remove a transient property for the player                     |
|prop, p            |`<real name> <property> [value]`   |Get(2) or Set(3) a persistent property for the player          |
|rmprop, rp         |`<real name> <property>`           |Remove a persistent property from the player                   |
|lookup, l          |`<real name>`                      |Look up a player, and display their data                       |
|loadtrans, lt      |`<real name>`                      |Reload the player's transient properties                       |
|loadperst, lp      |`<real name>`                      |Reload the player's persistent properties                      |
|lex, eval          |`<real name> <MVEL Expression>`    |Evaluent the expression in the context of the player           |

## Important information

### Friendship types

|Type               |Implications                           |
|:------------------|:--------------------------------------|
|`neutral`          |No tag colour, normal player           |
|`friend`           |Green tag, cannot be attacked directly |
|`enemy`            |Red tag, mods may prioritize           |

### Properties

Players may have any arbitrary number of properties; however, there are certain "special" properties, used directly by the internals of the friend system.

|Property           |Purpose                                |
|:------------------|:--------------------------------------|
|`player.nickname`  |Holds the player's nickname            |
|`player.friendship`|Holds the player's friendship level    |

## Chat Filtration

***Note: Referred to as `NameProtect` in the client***

***Note: In-Game name/nickname substitution is defacto***

This is enabled with `np`. While chat filtration is always ocurring, it only displays when this is on.

NameProtect uses two-pass substitution. First, incoming chat is replaced 1-for-1 player-to-nickname. After this, the message is split (preserving multiple spaces) and each word is similarity-matched to player names.
If a word is above the similarity threshold, it will be replaced with the nickname of that player.

`np` usage:

|Flag               |Arguments                          |Function                                                       |
|:------------------|:----------------------------------|:--------------------------------------------------------------|
|toggle, *@if_none* |                                   |Enable/Disable chat filtration                                 |
|similarity, s      |Double                             |Set similarity threshold for filtration                        |

### Finding string distance

Hysteria uses an implementation of the *Jaro-Winkler* string similarity algorithm, known for its use by the U.S. census in form correction.

It is exposed to the player via the `jaro` command.

`jaro` usage:

|Flag               |Arguments                          |Function                                                       |
|:------------------|:----------------------------------|:--------------------------------------------------------------|
|*@default*         |`<string> <string>`                |Check string distance                                          |
