# 1. Hysteria, a brief history
Hysteria is a minecraft client that targets griefers and administrators alike. It is unique in it's complex command syntax, modularity, and high configurability.
Hysteria has been under devlopment since late May 2011, making 2014 the third year of devlopment. Incidentally, this will make Hysteria one of the longest-developed minecraft hacks.
Hysteria is written by Bowser, a polyglot software engineer, who has been working on the project all through higschool.

## Hysteria's development timeline
- 2011
    - May: Hysteria is first released for Minecraft 1.7.5 (Beta), it uses modloader and is rather poorly written.
    - June~August: Beta 1.8, Bowser learns a lot more about java and starts moving towards design standards compliance
- 2012
    - Minecraft 1.9: No version of hysteria is released, nobody uses minecraft 1.9.
    - Minecraft RC: Hysteria for 1.0/1 comes out. This is the version that will get a good bit of publicity on H.F. during the multi-core troll that tied the whole griefing section up for 2-3 months.
    - Minecraft 1.2.5: Hysteria gets a major redesign. Adds spheres, the code for which was given to shusako and promptly became popular on H.F.
        - DashFoo project is started as a decoupled command parser
- 2013
    - Minecraft 1.3: Blasted summer job with idiots tied up Bowser
    - Minecraft 1.5: Bowser starts work on a java-based Hysteria with thoughts of releasing portions under OSS/Apache license.
- 2014
    - Minecraft 1.6: Bowser fucked around with MCP. Was busy with school.
    - Minecraft 1.7: Bowser is a second-semested Senior, and no longer gives a damn. Starts working on Hysteria again.

## Hype
Back in 1.1/1.2.5 Bowser and ZCS trolled the griefing section such that the griefing section will never be the same again, if I may claim so. We simply claimed we had created something we had known would be impossible, and the whole section got their panties in a knot over the whole damn affair.
With the release of Hysteria 8 for MC 1.7, there has been a lot of hype/bad jokes. Most of this stems from the fact that the majority of *<s>brick shitting</s>* griefing section still thinks that ZCS has either written or contributed code to Hysteria development.
Interestingly enough, many are ass-mad over the fact that the client is written in [Scala](http://scala-lang.org/).
